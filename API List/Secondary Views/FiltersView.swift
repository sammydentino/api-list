//
//  FiltersView.swift
//  API List
//
//  Created by Sammy Dentino on 7/26/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct FiltersView : View {
    let entries: [Entries]!
    let filters: [String]!
    @State private var showingAPI = false
    @State private var loading = true
    @State private var currentfilter = ""
    @State private var showingFilteredList = false
    
    var body: some View {
        NavigationView {
            List (filters) { filter in
                Button(action: {
                    self.currentfilter = filter.capitalized
                    self.showingFilteredList = true
                }) {
                    HStack {
                        Text(filter.capitalized).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary)
                        Spacer()
                        Text("⇢").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                    }
                }.sheet(isPresented: self.$showingFilteredList) {
                    NavigationView {
                        List (self.entries.filter({$0.category.lowercased().contains(self.currentfilter.lowercased())})) { entry in
                            Button(action: {
                                self.showingAPI = true
                            }) {
                                HStack {
                                    Text(entry.API).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                    Spacer()
                                    Text("⇢").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                }
                            }.sheet(isPresented: self.$showingAPI) {
                                DetailsView(entry: entry, action: {self.showingAPI = false})
                                .onDisappear(perform : {
                                    self.showingAPI = false
                                })
                            }
                        }.listStyle(GroupedListStyle())
                        .environment(\.horizontalSizeClass, .compact)
                        .navigationBarTitle(self.currentfilter)
                        .navigationBarItems(leading: Button(action: {
                            self.showingFilteredList = false
                        }) {
                            Text("← Back")
                                .font(.system(.subheadline, design: .rounded))
                                .bold()
                                .foregroundColor(.white)
                        })
                    }
                    .onDisappear(perform : {
                        self.showingFilteredList = false
                    })
                }
            }.listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, .compact)
            .navigationBarTitle("Categories")
        }
    }
}
