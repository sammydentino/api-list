//
//  SearchView.swift
//  API List
//
//  Created by Sammy Dentino on 7/26/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct SearchView : View {
    let entries: [Entries]!
    @State private var showingAPI = false
    @State private var loading = true
    @State private var search = ""
    
    var body: some View {
        NavigationView {
            VStack {
                SearchBar(text: $search).padding(.bottom, -7.5)
                List {
                    Section(header: Text("\(self.entries.filter({search.isEmpty ? true : $0.API.lowercased().contains(search.lowercased())}).count) Results").font(.system(size: 15, weight: .heavy, design: .rounded)).padding(.vertical, 7.5)) {
                        ForEach(entries.filter({search.isEmpty ? true : $0.API.lowercased().contains(search.lowercased())})) { entry in
                            Button(action: {
                                self.showingAPI = true
                            }) {
                                HStack {
                                    Text(entry.API).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                    Text("•  " + entry.category.capitalized).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.secondary).padding(.leading, -2)
                                    Spacer()
                                    Text("⇢").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                }
                            }.sheet(isPresented: self.$showingAPI) {
                                DetailsView(entry: entry, action: {self.showingAPI = false})
                                .onDisappear(perform : {
                                    self.showingAPI = false
                                })
                            }
                        }
                    }
                }.navigationBarTitle("Search")
            }
        }
    }
}
