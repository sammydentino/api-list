//
//  TabBar.swift
//  API List
//
//  Created by Sammy Dentino on 7/26/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct TabBar: View {
    @Binding var index: Int
    
    private var bottomPadding: CGFloat {
        if !UIDevice.current.hasNotch {
            return 15
        } else {
            return 0
        }
    }
    
    var body: some View {
        HStack(spacing: 10) {
            HStack {
                Image(systemName: "house.fill")
                    .resizable()
                    .frame(width: 22.5, height: 20)
                if self.index == 0 {
                    Text("Home").fontWeight(.medium).font(.system(size: 14)).padding(.trailing, 2.5)
                } else {
                    Text("").fontWeight(.medium).font(.system(size: 14))
                }
            }.padding(15)
                .background(self.index == 0 ? Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.5) : Color.clear)
                .clipShape(Capsule()).onTapGesture {
                    self.index = 0
            }.padding(.bottom, self.bottomPadding)
            HStack {
                Image(systemName: "book")
                    .resizable()
                    .frame(width: 20, height: 17.5)
                if self.index == 1 {
                    Text("All").fontWeight(.medium).font(.system(size: 14))
                } else {
                    Text("").fontWeight(.medium).font(.system(size: 14))
                }
            }.padding(15)
                .background(self.index == 1 ? Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.5) : Color.clear)
                .clipShape(Capsule()).onTapGesture {
                    self.index = 1
            }.padding(.bottom, self.bottomPadding)
            HStack {
                Image(systemName: "list.bullet")
                    .resizable()
                    .frame(width: 20, height: 17.5)
                if self.index == 2 {
                    Text("Categories").fontWeight(.medium).font(.system(size: 14))
                } else {
                    Text("").fontWeight(.medium).font(.system(size: 14))
                }
            }.padding(15)
                .background(self.index == 2 ? Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.5) : Color.clear)
                .clipShape(Capsule()).onTapGesture {
                    self.index = 2
            }.padding(.bottom, self.bottomPadding)
            HStack {
                Image(systemName: "magnifyingglass")
                    .resizable()
                    .frame(width: 20, height: 20)
                if self.index == 3 {
                    Text("Search").fontWeight(.medium).font(.system(size: 14))
                } else {
                    Text("").fontWeight(.medium).font(.system(size: 14))
                }
            }.padding(15)
                .background(self.index == 3 ? Color(red: 0.0353, green: 0.6941, blue: 0.9058).opacity(0.5) : Color.clear)
                .clipShape(Capsule()).onTapGesture {
                    self.index = 3
            }.padding(.bottom, self.bottomPadding)
        }.padding(.top, 8)
        .frame(width: UIScreen.main.bounds.width)
        .background(Color.white)
        .animation(.default)
    }
}

