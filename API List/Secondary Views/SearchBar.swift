//
//  SearchBar.swift
//  API List
//
//  Created by Sammy Dentino on 7/26/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct SearchBar: View {
    @Binding var text: String
 
    @State private var isEditing = false
 
    var body: some View {
        HStack {
            TextField("", text: $text)
                .font(.system(size: 15, weight: .medium, design: .rounded))
                .padding(7)
                .padding(.horizontal, 25)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .overlay(
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.gray)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 8)
                 
                        if isEditing {
                            Button(action: {
                                self.text = ""
                            }) {
                                Image(systemName: "multiply.circle.fill")
                                    .foregroundColor(.gray)
                                    .padding(.trailing, 8)
                            }
                        }
                    }
                )
                .padding(.horizontal, 10)
                .onTapGesture {
                    self.isEditing = true
                }
 
            if isEditing {
                Button(action: {
                    UIApplication.shared.endEditing()
                    self.isEditing = false
 
                }) {
                    Text("Done")
                        .font(.system(size: 0, weight: .heavy, design: .rounded))
                }
                .padding(.trailing, 17.25)
                .transition(.move(edge: .trailing))
                .animation(.default)
            }
        }.padding(.vertical, 10)
    }
}

