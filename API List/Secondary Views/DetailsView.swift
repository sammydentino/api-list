//
//  DetailsView.swift
//  API List
//
//  Created by Sammy Dentino on 7/23/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI
import ActivityIndicatorView

struct DetailsView: View {
    let entry : Entries!
    let action : () -> ()
    @State private var showingDetail = false
    @State private var loading = true
    
    var body: some View {
        NavigationView {
            VStack {
                ZStack {
                    if loading == true {
                        withAnimation {
                            VStack {
                                Spacer()
                                LinearGradient(gradient: Gradient(colors: [Color(red: 0.149, green: 0.9333, blue: 0.6314), Color(red: 0.0353, green: 0.6941, blue: 0.9058)]),
                                           startPoint: .top,
                                           endPoint: .bottom)
                                    .mask(ActivityIndicatorView(isVisible: .constant(true), type: .equalizer)).frame(width: 64.0, height: 64.0)
                                
                                Spacer()
                            }
                        }.animation(.spring())
                    } else {
                        withAnimation {
                            List {
                                Section(header: Text("\nDescription").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.secondary)) {
                                    Text(entry.description).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary).padding(.vertical, 5)
                                }
                                Section(header: Text("Category").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.secondary)) {
                                    Text(entry.category.capitalized).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                }
                                Section(header: Text("URL").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.secondary)) {
                                    if entry.link == "" {
                                        Text("N/A").font(.system(size: 15, weight: .medium, design: .rounded)).foregroundColor(.primary)
                                    } else {
                                        Button(action: {
                                            self.showingDetail.toggle()
                                        }) {
                                            HStack {
                                                Text(entry.link).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                                Spacer()
                                                Text("⇢").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                            }
                                        }.sheet(isPresented: self.$showingDetail) {
                                            NavigationView {
                                                WebView(request: URLRequest(url: URL(string: self.entry.link)!))
                                                    .navigationBarTitle(Text(self.entry.API), displayMode: .inline)
                                                    .navigationBarItems(leading: Button(action: {
                                                        self.showingDetail.toggle()
                                                    }) {
                                                        Text("← Back")
                                                            .font(.system(.subheadline, design: .rounded))
                                                            .bold()
                                                            .foregroundColor(.gray)
                                                    }
                                                )
                                            }
                                        }
                                    }
                                }
                                Section(header: Text("Authentication").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.secondary)) {
                                    if entry.auth == "" {
                                        Text("None").font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(entry.authcolor)
                                    } else {
                                        Text(entry.auth).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(entry.authcolor)
                                    }
                                }
                                Section(header: Text("HTTPS").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.secondary)) {
                                    Text(String(entry.HTTPS).capitalized).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(entry.httpscolor)
                                }
                                Section(header: Text("Cors").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.secondary)) {
                                    Text(entry.cors.capitalized).font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(entry.corscolor)
                                }
                            }.listStyle(GroupedListStyle())
                            .environment(\.horizontalSizeClass, .compact)
                        }.animation(.default).transition(.move(edge: .bottom))
                    }
                }.navigationBarTitle(entry.API)
                .navigationBarItems(leading: Button(action: {
                    self.action()
                }) {
                    Text("← Back")
                        .font(.system(.subheadline, design: .rounded))
                        .bold()
                        .foregroundColor(.white)
                })
            }
        }.onAppear(perform: {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                withAnimation {
                    self.loading = false
                }
            }
        })
    }
}
