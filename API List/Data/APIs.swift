//
//  APIs.swift
//  API List
//
//  Created by Sammy Dentino on 7/23/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct APIs : Codable {
    let count : Int!
    var entries : [Entries]!

    enum CodingKeys: String, CodingKey {
        case count = "count"
        case entries = "entries"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
        entries = try values.decodeIfPresent([Entries].self, forKey: .entries)
    }
}

struct Entries : Codable, Identifiable {
    let id = UUID()
    let API : String!
    var description : String!
    var auth : String!
    let HTTPS : Bool!
    let cors : String!
    let link : String!
    let category : String!
    var httpscolor : Color!
    var corscolor : Color!
    var authcolor : Color!

    enum CodingKeys: String, CodingKey {
        case API = "API"
        case description = "Description"
        case auth = "Auth"
        case HTTPS = "HTTPS"
        case cors = "Cors"
        case link = "Link"
        case category = "Category"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        API = try values.decodeIfPresent(String.self, forKey: .API) ?? "N/A"
        description = try values.decodeIfPresent(String.self, forKey: .description) ?? "N/A"
        auth = try values.decodeIfPresent(String.self, forKey: .auth) ?? "N/A"
        HTTPS = try values.decodeIfPresent(Bool.self, forKey: .HTTPS) ?? false
        cors = try values.decodeIfPresent(String.self, forKey: .cors) ?? "N/A"
        link = try values.decodeIfPresent(String.self, forKey: .link) ?? "N/A"
        category = try values.decodeIfPresent(String.self, forKey: .category) ?? "N/A"
        if description.components(separatedBy: " ").count >= 5 {
            if description.last != "." {
                description = description + "."
            } else if description.last != "?" {
                description = description + "."
            } else if description.last != "!" {
                description = description + "."
            }
        }
        if auth != "" {
            auth = "API Key Required"
        }
        if HTTPS {
            httpscolor = Color(.systemGreen)
        } else {
            httpscolor = Color(.systemRed)
        }
        if cors == "yes" {
            corscolor = Color(.systemGreen)
        } else {
            corscolor = Color(.systemRed)
        }
        if auth == "" {
            authcolor = Color(.systemOrange)
        } else {
            authcolor = Color(.systemGreen)
        }
    }
}
