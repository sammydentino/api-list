//
//  APIData.swift
//  API List
//
//  Created by Sammy Dentino on 7/23/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

class APIData: ObservableObject {
    @Published var apis : APIs!
    @Published var entries : [Entries]!
    @Published var isLoading : Bool = false
    @Published var categories = [String]()
    @Published var filters : [String]!
    
    init() {
        loadData()
        entries = entries.sorted(by: {
            $0.API < $1.API
        })
        for item in entries {
            if item.API[item.API.startIndex].isNumber == true {
                entries.removeFirst()
                entries.append(item)
            }
            categories.append(item.category.lowercased())
        }
        filters = Array(Set(categories))
        filters = filters.sorted(by: {
            $0 < $1
        })
    }
    
    func loadData() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/API-List/master/APIs.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode(APIs.self, from: d) {
                    apis = data
                    entries = data.entries
                }
            }
        }
    }
}
