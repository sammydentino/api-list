//
//  UIApplication.swift
//  API List
//
//  Created by Sammy Dentino on 7/26/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
