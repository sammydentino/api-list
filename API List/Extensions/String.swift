//
//  String.swift
//  API List
//
//  Created by Sammy Dentino on 7/26/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

extension String: Identifiable {
    public var id: String {
        return self
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

