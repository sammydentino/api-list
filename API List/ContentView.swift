//
//  ContentView.swift
//  API List
//
//  Created by Sammy Dentino on 7/23/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI
import WebKit
import ActivityIndicatorView

struct ContentView: View {
    @ObservedObject var data = APIData()
    @State private var showingAPI = false
    @State private var showingFilteredList = false
    @State private var selectedpage = 0
    @State private var search = ""
    @State private var loading = true
    @State private var currentfilter = ""
    
    let coloredNavAppearance = UINavigationBarAppearance()
    init() {
        let design = UIFontDescriptor.SystemDesign.rounded
        let descriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .largeTitle).withDesign(design)!
        let font = UIFont.init(descriptor: descriptor, size: 48)
        UINavigationBar.appearance().largeTitleTextAttributes = [.font : font.bold()]
        coloredNavAppearance.configureWithOpaqueBackground()
        coloredNavAppearance.backgroundColor = UIColor(red: 0.0353, green: 0.6941, blue: 0.9058, alpha: 1.0)
        coloredNavAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        coloredNavAppearance.largeTitleTextAttributes = [.font : font.bold(), .foregroundColor: UIColor.white]

        UINavigationBar.appearance().standardAppearance = coloredNavAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredNavAppearance
    }
    
    var body: some View {
        VStack {
            ZStack {
                if self.selectedpage == 0 {
                    NavigationView {
                        VStack {
                            List {
                                HStack {
                                    Spacer()
                                    Image("List")
                                        .resizable()
                                        .frame(width: 192, height: 192)
                                    Spacer()
                                }.padding(.vertical).padding(.bottom).padding(.top, 7.5)
                                Section(header: Text("\nQuick Navigation").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.secondary)) {
                                    Button(action: {
                                        self.selectedpage = 1
                                    }) {
                                        HStack {
                                            Text("All APIs").font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                            Spacer()
                                            Text("⇢").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                        }
                                    }
                                    Button(action: {
                                        self.selectedpage = 2
                                    }) {
                                        HStack {
                                            Text("APIs by Category").font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                            Spacer()
                                            Text("⇢").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                        }
                                    }
                                    Button(action: {
                                        self.selectedpage = 3
                                    }) {
                                        HStack {
                                            Text("Search APIs").font(.system(size: 15, weight: .bold, design: .rounded)).foregroundColor(.primary)
                                            Spacer()
                                            Text("⇢").font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(.primary)
                                        }
                                    }
                                }
                            }.listStyle(GroupedListStyle())
                            .environment(\.horizontalSizeClass, .compact)
                        }.navigationBarTitle("API List")
                    }
                } else if self.selectedpage == 1 {
                    AllAPIView(entries: self.data.entries)
                } else if self.selectedpage == 2 {
                    FiltersView(entries: self.data.entries, filters: self.data.filters)
                } else if self.selectedpage == 3 {
                    SearchView(entries: self.data.entries)
                }
            }
            TabBar(index: $selectedpage)
        }.animation(.spring())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
